﻿#pragma checksum "..\..\HomeScreen.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "E70F26CD85870A1FD832A34711CC230F"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace To_do_Prototype {
    
    
    /// <summary>
    /// HomeScreen
    /// </summary>
    public partial class HomeScreen : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\HomeScreen.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas TopIcons;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\HomeScreen.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid PointsContainer;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\HomeScreen.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image image;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\HomeScreen.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label PointsText;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\HomeScreen.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgSync;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\HomeScreen.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgCalender;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\HomeScreen.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgSort;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\HomeScreen.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lsvSortBy;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\HomeScreen.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lsvViewBy;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\HomeScreen.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel TaskSection;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\HomeScreen.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgHome;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\HomeScreen.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgToday;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\HomeScreen.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgComplete;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\HomeScreen.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgAdd;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/To-do Prototype;component/homescreen.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\HomeScreen.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.TopIcons = ((System.Windows.Controls.Canvas)(target));
            return;
            case 2:
            this.PointsContainer = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.image = ((System.Windows.Controls.Image)(target));
            return;
            case 4:
            this.PointsText = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.imgSync = ((System.Windows.Controls.Image)(target));
            return;
            case 6:
            this.imgCalender = ((System.Windows.Controls.Image)(target));
            
            #line 20 "..\..\HomeScreen.xaml"
            this.imgCalender.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.imgCalender_MouseDown);
            
            #line default
            #line hidden
            return;
            case 7:
            this.imgSort = ((System.Windows.Controls.Image)(target));
            
            #line 21 "..\..\HomeScreen.xaml"
            this.imgSort.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.imgSort_MouseDown);
            
            #line default
            #line hidden
            return;
            case 8:
            this.lsvSortBy = ((System.Windows.Controls.ListView)(target));
            return;
            case 9:
            
            #line 23 "..\..\HomeScreen.xaml"
            ((System.Windows.Controls.ListViewItem)(target)).MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.lviAlphabetical);
            
            #line default
            #line hidden
            
            #line 23 "..\..\HomeScreen.xaml"
            ((System.Windows.Controls.ListViewItem)(target)).Selected += new System.Windows.RoutedEventHandler(this.lviAlphabetical);
            
            #line default
            #line hidden
            return;
            case 10:
            
            #line 24 "..\..\HomeScreen.xaml"
            ((System.Windows.Controls.ListViewItem)(target)).Selected += new System.Windows.RoutedEventHandler(this.lviDueDate);
            
            #line default
            #line hidden
            return;
            case 11:
            
            #line 25 "..\..\HomeScreen.xaml"
            ((System.Windows.Controls.ListViewItem)(target)).Selected += new System.Windows.RoutedEventHandler(this.vbiCategory);
            
            #line default
            #line hidden
            return;
            case 12:
            this.lsvViewBy = ((System.Windows.Controls.ListView)(target));
            return;
            case 13:
            
            #line 28 "..\..\HomeScreen.xaml"
            ((System.Windows.Controls.ListViewItem)(target)).Selected += new System.Windows.RoutedEventHandler(this.lviAgenda);
            
            #line default
            #line hidden
            return;
            case 14:
            
            #line 29 "..\..\HomeScreen.xaml"
            ((System.Windows.Controls.ListViewItem)(target)).Selected += new System.Windows.RoutedEventHandler(this.lviMonthly);
            
            #line default
            #line hidden
            return;
            case 15:
            this.TaskSection = ((System.Windows.Controls.StackPanel)(target));
            
            #line 34 "..\..\HomeScreen.xaml"
            this.TaskSection.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.TaskSection_MouseDown);
            
            #line default
            #line hidden
            return;
            case 16:
            this.imgHome = ((System.Windows.Controls.Image)(target));
            
            #line 47 "..\..\HomeScreen.xaml"
            this.imgHome.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.imgHome_MouseDown);
            
            #line default
            #line hidden
            return;
            case 17:
            this.imgToday = ((System.Windows.Controls.Image)(target));
            
            #line 48 "..\..\HomeScreen.xaml"
            this.imgToday.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.imgToday_MouseDown);
            
            #line default
            #line hidden
            return;
            case 18:
            this.imgComplete = ((System.Windows.Controls.Image)(target));
            
            #line 49 "..\..\HomeScreen.xaml"
            this.imgComplete.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.imgComplete_MouseDown);
            
            #line default
            #line hidden
            return;
            case 19:
            this.imgAdd = ((System.Windows.Controls.Image)(target));
            
            #line 50 "..\..\HomeScreen.xaml"
            this.imgAdd.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.AddNewTask);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

